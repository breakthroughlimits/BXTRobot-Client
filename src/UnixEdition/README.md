## BXTRobot UnixEdition 使用方法

Step1. 首先，用 Git克隆 （或者点击下载），如图：

> Git 克隆命令：

```
git clone https://git.oschina.net/Lindor_L/BXTRobot-Client.git

```

> 直接下载解压：

![](./imgs/1.png)


Step2. 找到"src/UnixEdition"目录下的"brobot.sh"文件，填入账号和密码：

![](./imgs/2.png)

Step3. 填入账号和密码后使用命令行目录运行"brobot.sh"：

```
./brobot.sh

```
