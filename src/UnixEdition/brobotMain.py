# -*- coding: utf-8 -*-
"""
百晓通客栈人工智能助理——BXTRobot，主程序代码
Powered by www.bxtkezhan.cc

微信公众号：bxtkezhan，认准中文名“百晓通客栈”

QQ群：技术交流（325237077，自由编码）；项目交流（431646132，创客项目）

"""
from Tkinter import *
import getJson
import subprocess
import sys

class BRobot():
    """BRobot 类，定义了主窗口，窗口控件，窗口事件"""
    def __init__(self, user_name, user_passwd, master):
        # 设置根窗口.
        self.USER_NAME = user_name	# 接收用户名
        self.USER_PASSWD = user_passwd	# 接收用户密码
        self.master = master
        self.master.title(' '*65 + 'BXTRobot')	# 置窗口题目内容
        self.master.geometry('720x400')	# 设置主窗口大小
        self.master.resizable(width=False, height=False)	# 禁止改版窗口大小

        # 设置根框架.
        self.rootFrame = Frame(self.master)

        # 设置上框架.
        self.topFrame = Frame(self.rootFrame, bg='#444444', padx=5, pady=5)

		# 设置上框架的TEXT控件及取消其输入功能.
        self.scrollbar = Scrollbar(self.topFrame)
        self.scrollbar.pack(side=RIGHT, fill=Y)
        self.topText = Text(self.topFrame, height=16,
                            font=('DejaVu Sans Mono', 12), bg='#444444',
                            yscrollcommand=self.scrollbar.set)
        self.topText.bind('<Key>', lambda event:'break')	# 取消文本输入功能
        self.scrollbar.config(command=self.topText.yview)
        self.topText.tag_config('tag_you', foreground='#3465a4')
        self.topText.tag_config('tag_brobot', foreground='#a75e00')
        self.topText.pack()

        self.topFrame.pack(side=TOP)

        # 设置下框架.
        self.bottomFrame = Frame(self.rootFrame, bg='#444444', padx=5, pady=5)

		# 设置下框架的Entry框架及其按键响应.
        self.bottomEntryVar = StringVar()
        self.bottomEntry = Entry(self.bottomFrame, textvariable=self.bottomEntryVar,
                                 font=('DejaVu Sans Mono', 13), bg='#e7eeee')
        self.bottomEntryVar.set('')
        self.bottomEntry.pack(side=TOP, fill=X)
        self.bottomEntry.bind('<Key>', self.sendToRobotEnt)

		# 设置下框架的Button控件及其事件响应.
        Button(self.bottomFrame,
               text='Send', width=80,
               font=('Mono', 12), fg='#eedaad', bg='#444444',
               command=self.sendToRobot).pack(side=BOTTOM)
        self.bottomFrame.pack(side=TOP)

        self.rootFrame.pack()

        # 判断是否需要用户选择，为0时不用选择，为1时需要选择.
        self.TO_SELECT = 0

        # 使用的工具类型.
        self.USE_TOOL = None

        # 当工具参量“self.USE_TOOL”为命令行shell的时候，选择填入的参数列表.
        self.POPEN_LIST = None

    # 回调函数，当点击Button（Send）的时候触发.
    def sendToRobot(self):
        send_str = self.bottomEntryVar.get()
        if len(send_str) >= 1:
            if self.TO_SELECT == 0:
                # Your says.
                self.topText.insert(END, 'You Say:>>> ' + send_str + '\n', 'tag_you')
                # Debug code.
                # print 'TO_SELECT is None(0)'

                json_map= getJson.sendWord(self.USER_NAME, self.USER_PASSWD, 
                                           send_str.encode('utf-8'))
                # Debug code.
                print json_map

                if type(json_map) == dict:
                    # 工具json_map的code值选用内容处理函数.
                    if json_map['code'] == 100000:
                        self.code_100000(json_map['text'])
                    elif json_map['code'] == 200000:
                        self.code_200000(json_map['text'], json_map['url'])
                else:
                    self.topText.insert(END, 'You Say:>>> ' + '对不起，我没有听到...' + '\n', 'tag_brobot')

            elif self.TO_SELECT == 1:
                # Your says.
                self.topText.insert(END, 'You Say:>>> ' + send_str + '\n', 'tag_you')
                # Debug code.
                # print 'TO_SELECT is To select(1)'

                # 根据用户输入判断使用什么工具完成什么命令.
                if send_str == 'Y' or send_str == 'y':
                    # Debug code.
                    # print 'select Y'
                    if self.USE_TOOL == 'CMD':
                        subprocess.Popen(self.POPEN_LIST)

                self.TO_SELECT = 0

            # 设置上方文本框TEXT的显示位置，清楚下方Entry控件的内容.
            self.topText.see(END)
            self.bottomEntryVar.set('')
        else:
            self.topText.insert(END, 'You Say:>>> ' + '你说呀!' + '\n', 'tag_brobot')
	
	# 按键回调函数，当下方Entry中接收回车建的时候触发.
    def sendToRobotEnt(self, event):
        if event.char == '\r':
            self.sendToRobot()

	# 返回json_map的code值为100000时调用
    def code_100000(self, text):
        self.topText.insert(END, 'BRobot:>>> ' + text.replace('<br>', '\nBRobot:>>> ')\
                            + '\n', 'tag_brobot')

	# 返回json_map的code值为200000时调用
    def code_200000(self, text, url):
        self.code_100000(text)
        self.TO_SELECT = 1
        self.USE_TOOL = 'CMD'
        self.POPEN_LIST = ['google-chrome', url]
        self.topText.insert(END, 'BRobot:>>> ' + '那么，是否用浏览器打开呢？'\
                            + '\n', 'tag_brobot')
        self.topText.insert(END, 'BRobot:>>> ' + '输入【Y/y】确认，输入其它则退出！'\
                            + '\n', 'tag_brobot')
        
if __name__ == '__main__':
    if len(sys.argv) == 3:
        if type(getJson.sendWord(sys.argv[1], sys.argv[2], 'hi')) == dict:
            root = Tk()
            brobot = BRobot(sys.argv[1], sys.argv[2], root)
            root.mainloop()
        else:
            print 'Login False'
    else:
        print 'Please input username and password.'
