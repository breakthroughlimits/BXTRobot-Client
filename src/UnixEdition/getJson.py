# -*- coding: utf-8 -*-
import urllib2
import urllib
import json

def sendWord(user_name, user_passwd, word):
    url = 'http://120.25.255.166/brobot_api'

    data = {}
    data['name_or_email'] = user_name
    data['user_passwd'] = user_passwd
    data['user_word'] = word
    data = urllib.urlencode(data).encode('utf-8')

    req = urllib2.Request(url, data)
    res = None
    try:
        res = urllib2.urlopen(req, timeout = 5)
    except Exception as e:
        print 'Error: %r' % e
        return None

    try:
        json_str = res.read().decode('utf-8', 'ignore')
        for i in ('loss_mess', 'no_login', 'sql_exist'):
            if i in json_str:
                return None
    except Exception as e:
        print 'Error: %r' % e
        return None
    finally:
        res.close()

    return json.loads(json_str)

# Debug code.
if __name__ == '__main__':
    print sendWord('', '', '')
    
